package com.example.made.Model

data class Hero (
    var photo: Int,
    var name: String,
    var description: String
)